var numero = Math.floor(Math.random() * 50) + 1;
window.onload = function(){
        for (var i = 1; i <= 50; i++){
        //Creamos el boton
        var boton = document.createElement("button");
        //Añadirle un id
        boton.id = "boton" + i;
        //Le damos un valor al boton
        boton.innerHTML = i;
        boton.style.width = '50px';
        boton.style.marginRight = '10px';
        boton.style.marginBottom = '10px';
        boton.className = "raise hover";
        boton.addEventListener('click', Adivina, false);
        //Lo añadimos al elemento con id = "Botones"
        document.getElementById('Botones').appendChild(boton);
        boton.value = i;
        if( i % 10 == 0){
            var salto = document.createElement("br");
            document.getElementById('Botones').appendChild(salto);
        }
        if(i == 50){
            var span = document.createElement("span");
            span.id = 'fin';
            document.getElementById('Botones').appendChild(span);
        }
        
    }
}
function Adivina(){
    if(this.value > numero){
        for(var i = this.value; i <= 50; i++){
            document.getElementById('boton' + i).style.color = '#e5ff60';
            document.getElementById('boton' + i).disabled = true;
            document.getElementById('boton' + i).classList.remove('hover');
        }

    }else if(this.value < numero){
        for(var i = this.value; i >= 1; i--){
            document.getElementById('boton' + i).style.color = '#e5ff60';
            document.getElementById('boton' + i).disabled = true;
            document.getElementById('boton' + i).classList.remove('hover');
        }

    }else {
        for(var i = 1; i <= 50; i++){
            document.getElementById('boton' + i).disabled = true;
            document.getElementById('boton' + i).classList.remove('hover');
        }

        document.getElementById('boton' + this.value).style.color = '#58a0e4';
        document.getElementById('fin').innerHTML = 'Has Ganado';
    }
}