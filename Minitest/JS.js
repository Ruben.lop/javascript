var aciertos = [false, false, false, false];

function CompruebaTest(f) {
    if (f.a1.checked) {
        document.getElementById('headingOne').style.background = 'green';
        aciertos[0] = true;
    }
    else {
        document.getElementById('headingOne').style.background = 'red';
        aciertos[0] = false;
    }

    var contador = 0;
    var respuestas = 0;
    for (var i = 1; i < 4; i++) {

        if (document.getElementById('b' + i).checked) {
            contador += (Number)(document.getElementById('b' + i).value);
            respuestas++;
        }
    }

    if (contador == 2 && respuestas == 2) {
        document.getElementById('headingTwo').style.background = 'green';
        aciertos[1] = true;
    }
    else if (contador == 1 || contador == 2) {
        document.getElementById('headingTwo').style.background = 'orange';
        aciertos[1] = false;
    }
    else {
        document.getElementById('headingTwo').style.background = 'red';
        aciertos[1] = false;
    }

    if (f.c1.value == 1) {
        document.getElementById('headingThree').style.background = 'green';
        aciertos[2] = true;
    }
    else {
        document.getElementById('headingThree').style.background = 'red';
        aciertos[2] = false;
    }

    if (/^TRUNCATE TABLE$/ig.test(f.text.value)) {
        document.getElementById('headingFour').style.background = 'green';
        aciertos[3] = true;
    }
    else {
        document.getElementById('headingFour').style.background = 'red';
        aciertos[3] = false;
    }

    var contadorAciertos = 0;
    for (var i = 0; i < aciertos.length; i++) {
        if (aciertos[i]) {
            contadorAciertos++;
        }
    }

    var porcentaje = (contadorAciertos / aciertos.length) * 100;
    document.getElementById('resultado').innerHTML = 'Porcentaje: ' + porcentaje + '%';

    if (porcentaje == 100) {
        f.final.disabled = true;
    }

    f.final.value = 'Reintentar';
}