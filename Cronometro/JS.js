var horas = '00';
var minutos = '00';
var segundos = '00';
var mili = '00';
var crono;
var pause = true;
var contador = 0;
window.onload = function () {
    document.getElementById('hora').innerHTML = horas + ':' + minutos + ':' + segundos + ':' + '<span>' + mili + '</span>';
}

function Play() {
    if(pause){
        crono = setInterval(Cronometro, 10);
        pause = false;
    }
}

function Pause() {
    clearInterval(crono);
    pause = true;
}

function Stop() {
    horas = '00';
    minutos = '00';
    segundos = '00';
    mili = '00';
    clearInterval(crono);
    pause = true;
    document.getElementById('hora').innerHTML = horas + ':' + minutos + ':' + segundos + ':' + '<span>' + mili + '</span>';
}

function Vuelta() {
    if (!pause) {
        if(contador == 5){
            contador = 0;
            document.getElementById('tiempos').innerHTML = '';
        }
        document.getElementById('tiempos').innerHTML += horas + ":" + minutos + ":" + segundos + ":" + "<span class='tiempos'>" + mili + "</span>" + "<br>";
        horas = '00';
        minutos = '00';
        segundos = '00';
        mili = '00';
        contador++;
    }
}

function Cronometro() {
    if (mili == 100) {
        segundos++;
        mili = 0;
    }
    if (segundos == 60) {
        minutos++;
        segundos = 0;
    }
    if (minutos == 60) {
        horas++;
        minutos = 0;
    }

    if (mili < 10 && mili.toString().length < 2) {
        mili = '0' + mili;
    }
    if (segundos < 10 && segundos.toString().length < 2) {
        segundos = '0' + segundos;
    }
    if (minutos < 10 && minutos.toString().length < 2) {
        minutos = '0' + minutos;
    }
    if (horas < 10 && horas.toString().length < 2) {
        hors = '0' + horas;
    }

    document.getElementById('hora').innerHTML = horas + ':' + minutos + ':' + segundos + ':' + '<span>' + mili + '</span>';
    mili++
}